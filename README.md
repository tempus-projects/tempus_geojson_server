# Tempus GeoJSON server

This is a Flask application that presents a web api to query tempus and serves GeoJSON with the results.


## Getting started with Tempus

This repo depends on [pytempus](https://gitlab.com/Oslandia/pytempus) (and
therefore on [tempus-core](https://gitlab.com/Oslandia/tempus_core)). It also
supposes you have a populated tempus database (so you might want to check
[tempus_loader](https://gitlab.com/Oslandia/tempus_loader))

For your convenience, you can get all these repos by cloning the [Tempus master
repo](https://github.com/Ifsttar/Tempus).

If you are interested in complete demos, please check the [tempus-demos
repo](https://gitlab.com/Oslandia/tempus_demos).


## How to test it

If you are interested in ready-made demo, you should head to [tempus_demos
repo](https://gitlab.com/Oslandia/tempus_demos).

### Install dependencies

Only python3 is supported.

**Prerequisite**: Clone and build
[tempus-core](https://gitlab.com/Oslandia/tempus_core), because we depend on
pytempus, which in turn needs tempus-core.

Create a virtualenv and activate it:

```bash
virtualenv venv --python=python3
source venv/bin/activate
```

Then install python dependencies

```bash
pip install .
```

If you installed tempus_core in an unusual location, you need to play with -I
and -L options to `python setup.py build_ext`. It's possible to put them in a
requirements.txt files to make pip pass them to setup.py.

### Run it

Then make sure `LD_LIBRARY_PATH` is correctly set to find tempus-core binaries. It
should point to `<path-to-your-tempus-install-dir>/lib:<path-to-your-tempus-install-dir>/lib/tempus`. If installed globally, you don't need to do anything.

`<path-to-your-tempus-install-dir>` is usually `/usr/local`.

Write a configuration file (e.g. `default.cfg`) in the root project
directory. This file is mandatory to have a database connection. You can also
use the environment variable `TEMPUS_APP_SETTINGS` which specify the
configuration file to use. See the `example.cfg` file.

Make sure you have a correctly populated database.

```bash
FLASK_APP=webtempus/app.py flask run
```

or maybe:

```bash
TEMPUS_APP_SETTINGS=demo.cfg FLASK_APP=webtempus/app.py flask run
```


## Dump a Graph

Before launching the app, you can dump a tempus graph into a file. You must have
the `graph_dumper` command. Then:

`graph_dumper --db "dbname=[dbname] user=[user] host=[host]" --file demo_tempus.bin`

The values of `dbname, user` and `host` should be the same as in your
configuration file.

**Benefits**: the loading of the tempus graph when you start the web application
is much faster and the memory footprint should be way lower.

To use the generated file, you can edit your configuration file with:

```
GRAPH_FILE = "demo_tempus.bin"
```

You can put it where you launch the web app or specify an absolute file path.
