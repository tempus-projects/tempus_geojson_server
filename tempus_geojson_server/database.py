# -*- coding: utf-8 -*-

"""
Simple psycopg2 wrapper
"""

import psycopg2
import psycopg2.pool
from psycopg2.extras import RealDictCursor
from contextlib import contextmanager

INSTANCE = None
def getDbInstance(config):
    global INSTANCE
    if INSTANCE is None:
        INSTANCE = Database(config)
    return INSTANCE


class Database:
    def __init__(self, config):
        self.pool = psycopg2.pool.SimpleConnectionPool(1, config['DBMAXCONN'],
                                database=config['DBNAME'],
                                user=config['DBUSER'],
                                password=config.get('DBPASSWORD', None),
                                port=config.get('DBPORT', 5432),
                                host=config.get('DBHOST', 'localhost'))

    @contextmanager
    def cursor(self, cursor_factory=RealDictCursor):
        conn = self.pool.getconn()
        try:
            yield conn.cursor(cursor_factory=cursor_factory)
        finally:
            self.pool.putconn(conn)
