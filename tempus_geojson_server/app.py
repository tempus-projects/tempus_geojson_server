# -*- coding: utf-8 -*-

import base64
from datetime import datetime
import os
import sys

from geomet import wkb
from flask import Flask, request, jsonify

import pytempus as tempus

from .database import getDbInstance
from . import load

# root project directory
_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))


def add_cors(app):
    """Add Cross-Origin Resource Sharing

    Only used in DEBUG mode.
    """
    try:
        from flask_cors import CORS

        CORS(app, supports_credentials=True)
    except ImportError:
        print('WARNING!! DEBUG mode on, but I couldn\'t import CORS module.\nPlease install flask_cors if you want cors headers to be added automatically')


# init webapp
app = Flask(__name__)


def find_vertex_id(coordinates):
    """
    Parse str from request and find associated vertex id

    coordinates: list
        Pair of float
    config: dict
        DB configuration
    """
    query = 'SELECT tempus.road_node_id_from_coordinates({}, {})'.format(coordinates[0], coordinates[1])
    with database.cursor() as cur:
        cur.execute(query)
        res = cur.fetchall()
    if not res:
        return None

    return res[0]['road_node_id_from_coordinates']

def parse_coordinates(coordinates):
    """Return the floating coordinates from a string
    """
    x, y = coordinates.split(';')
    return [float(x), float(y)]

def to_geoJSON(tempusResult):
    """
    Transform a tempus result (either roadmap or isochrone) into a geojson object.

    Roadmaps are represented by a featureCollection where each feature is a step in the itinerary

    Isochrone is yet to be implemented
    """

    # A roadmap is a feature collection, and each part of the itinerary is a
    # feature. Therefore, a complete tempus answer containing several roadmaps
    # won't be a GeoJSon object, but an array of such objects. It's a necessary
    # trade-off, because in geojson a single coherent entity is a feature (ie a
    # geometry + some metadatas). I wish FeatureCollections could contain other
    # FeatureCollections but...
    #
    # Concretely, this has the advantage of being able to style
    # each part of the itinerary separately, as most libraries style's granularity
    # is the feature.
    transport_modes = get_transportmodes()
    generalProperties = {}
    geoObj = { 'type': 'FeatureCollection', 'features': [], 'properties': generalProperties }
    if tempusResult.is_roadmap():
        rm = tempusResult.roadmap()
        generalProperties['starting_date_time'] = rm.starting_date_time
        for s in rm:
            properties = {}
            feature = { 'type': 'Feature', 'properties': properties }

            ## GENERAL PROPERTIES
            # geometry
            geometry = wkb.loads(base64.b16decode(s.geometry_wkb.upper()))
            feature['geometry'] = geometry
            # type
            properties['step_type'] = s.step_type.name
            # Transport mode
            properties['transport_mode'] = [ tm for tm in transport_modes if tm['id'] == s.transport_mode][0]

            if s.step_type == tempus.Roadmap.Step.StepType.RoadStep:
                properties['end_movement'] = s.end_movement.name
                properties['road_name'] = s.road_name
                properties['distance_km'] = s.distance_km

            if s.step_type == tempus.Roadmap.Step.StepType.RoadStepTransferStep:
                properties['initial_name'] = s.initial_name
                properties['final_name'] = s.final_name
                properties['final_mode'] = s.final_mode

            if s.step_type == tempus.Roadmap.Step.StepType.PublicTransportStep:
                properties['network_id'] = s.network_id
                properties['wait'] = s.wait
                properties['departure_time'] = s.departure_time
                properties['arrival_time'] = s.arrival_time
                properties['trip_id'] = s.trip_id
                properties['departure_stop'] = s.departure_stop
                properties['departure_name'] = s.departure_name
                properties['arrival_stop'] = s.arrival_stop
                properties['arrival_name'] = s.arrival_name
                properties['route'] = s.route

            # costs
            properties['costs'] = {}
            for costType, costValue in s.costs().iteritems():
                properties['costs'][costType.name] = costValue

            geoObj['features'].append(feature)
    else:
        # TODO
        print('Found an isochrone, translation to geojson not yet supported')

    return geoObj


@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route("/testdb")
def testdb():
    q = "select count(1) from tempus.road_section"
    with database.cursor() as cu:
        cu.execute(q)
        return jsonify(cu.fetchall())

def get_transportmodes():
    q = "select id, name from tempus.transport_mode"
    with database.cursor() as cu:
        cu.execute(q)
        return cu.fetchall()

@app.route("/list_transportmodes")
def list_transportmodes():
    return jsonify(get_transportmodes())

@app.route('/request', methods=['GET', 'POST'])
def make_request():
    """
    Request an itinerary from tempus. Requests can be either GET or POST, and
    should contains 2 parameters: start and dest, of format 'x;y'
    """

    # Parse request arguments
    if request.method == 'POST':
        dataContainer = request.form
    elif request.method == 'GET':
        dataContainer = request.args
    else:
        return 'method not allowed', '405'

    try:
        startCoordinates = parse_coordinates(dataContainer['start'])
        destCoordinates = parse_coordinates(dataContainer['dest'])
    except ValueError as err:
        print('Error: cannot parse coordinates', err.message)
        return 'Cannot parse coordinates in request', '400'

    # Find the vertex id
    startVertexId = find_vertex_id(startCoordinates)
    destVertexId = find_vertex_id(destCoordinates)
    print("start vertex id: {}".format(startVertexId))
    print("dest vertex id: {}".format(destVertexId))

    if not startVertexId:
        return 'Cannot find start point in DB!', '422'
    if not destVertexId:
        return 'Cannot find dest point in DB!', '422'

    tempusReq = tempus.Request()

    tempusReq.origin = startVertexId
    dest = tempus.Request.Step()
    dest.location = destVertexId

    # allowedmodes
    if not dataContainer.has_key('allowed_modes'):
        return 'You must specify at least one value in allowed_modes', '400'

    try:
        for mode in dataContainer.getlist('allowed_modes'):
            tempusReq.add_allowed_mode(long(mode))
    except ValueError as err:
        print('Error: cannot parse allowed_modes id', err.message)
        return 'Cannot parse id in allowed_modes', '400'

    # private_vehicule_at_destination
    if dataContainer.get('private_vehicule_at_destination') == 'on':
        dest.private_vehicule_at_destination = True

    # Constraints
    if not dataContainer.get('constraint_type') == 'NoConstraint':
        timeConstraint = tempus.Request.TimeConstraint()
        try:
            timeConstraint.type = tempus.Request.TimeConstraintType.names[dataContainer.get('constraint_type')]
        except KeyError:
            return 'Cannot find constraint type {}'.format(dataContainer.get('constraint_type')), '400'
        try:
            timeConstraint.date_time = datetime.strptime(dataContainer.get('time_constraint'), '%d/%m/%Y %H:%M')
        except ValueError:
            return 'Cannot parse date and time {}. Expected format is DD/MM/YY HH:MM'.format(dataContainer.get('time_constraint')), '400'
        dest.constraint = timeConstraint

    tempusReq.set_destination_step(dest)
    result = load.plugin.request(tempusReq)

    return jsonify({"roadmaps": [ to_geoJSON(r) for r in result]})

# read conf file
if 'TEMPUS_APP_SETTINGS' not in os.environ:
    print("Reading the configuration file default.cfg")
    filepath = os.path.join(_ROOT, 'default.cfg')
    if not os.path.isfile(filepath):
        print("Please provide a default.cfg file or set the TEMPUS_APP_SETTINGS env var")
        sys.exit(0)
    app.config.from_pyfile(filepath)
else:
    app.config.from_envvar('TEMPUS_APP_SETTINGS')

# add CORS only in debug mode
if app.config['DEBUG']:
    add_cors(app)

# Initiate the connexion pool
database = getDbInstance(app.config)
# load the tempus plugin (just once)
load.plugin = load.PluginManager(app.config['TEMPUS_PLUGIN'], app.config)
load.plugin.instance()

